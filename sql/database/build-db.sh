#!/bin/bash
set -e

if [[ "$POSTGRES_USER" != "postgres" || "$POSTGRES_DB" != "postgres" ]]; then
	echo "ERROR: POSTGRES_USER and POSTGRES_DB environment variables must be equal to 'postgres'"
	exit 1
fi

if [[ -z "$FDW_DATABASE" || -z "$FDW_HOST" || -z "$FDW_PASSWORD" || -z "$FDW_USER" ]]; then
	echo "ERROR: FDW_DATABASE, FDW_HOST, FDW_PASSWORD and FDW_USER must be defined and initialized."
	exit 1
fi

gnucash_reporting_database="gnucash_reporting"
gnucash_reporting_user="gnucash_reporting"

# psql -a -d "${POSTGRES_DB}" -U "${POSTGRES_USER}" -c "
# DROP DATABASE IF EXISTS ${gnucash_reporting_database};"

psql -a -d "${POSTGRES_DB}" -U "${POSTGRES_USER}" -c "
CREATE DATABASE ${gnucash_reporting_database} WITH OWNER = ${POSTGRES_USER} CONNECTION LIMIT = -1;"

schemas=(dba reporting gnucash)

for schema in "${schemas[@]}"; do

	psql -a -d ${gnucash_reporting_database} -U "${POSTGRES_USER}" -c "CREATE SCHEMA ${schema};"

done

psql -a -d ${gnucash_reporting_database} -U "${POSTGRES_USER}" -c "
CREATE EXTENSION pg_stat_statements SCHEMA reporting;
CREATE EXTENSION tablefunc SCHEMA reporting;
CREATE EXTENSION postgres_fdw;"

psql -a -d ${gnucash_reporting_database} -U "${POSTGRES_USER}" -c "
CREATE SERVER gnucash_app FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host '${FDW_HOST}', dbname '${FDW_DATABASE}', updatable 'false');
CREATE USER MAPPING FOR postgres SERVER gnucash_app OPTIONS (user '${FDW_USER}', password '${FDW_PASSWORD}');
IMPORT FOREIGN SCHEMA public LIMIT TO (commodities, accounts, prices, splits, transactions) FROM SERVER gnucash_app INTO gnucash;"

# materialized views derived directly from the gnucash 'book' base tables
base_views=(
	"./views/reporting.commodities.sql"
	"./views/reporting.accounts.sql"
	"./views/reporting.prices.sql"
	"./views/reporting.splits.sql"
	"./views/reporting.transactions.sql"
)

for baseview in "${base_views[@]}"; do

	psql -d ${gnucash_reporting_database} -U "${POSTGRES_USER}" -f "${baseview}"

done

# functions
routines=(
	"./functions/dba.fn_refresh_reporting.sql"
	"./functions/reporting.fns_lastdaymonth.sql"
	"./functions/reporting.fnt_midmonthandmonthenddates.sql"
	"./functions/reporting.fns_mintrandate.sql"
	"./functions/reporting.fnt_nearestcommodityprice.sql"
	"./functions/reporting.fnt_portfoliobasisvalue.sql"
	"./functions/reporting.fnt_rpt_assets_allocation.sql"
	"./functions/reporting.fnt_rpt_expense_detail_fuel_usage.sql"
	"./functions/reporting.fnt_rpt_portfolio.sql"
	"./functions/reporting.fnt_rpt_portfolio_summary.sql"
	"./functions/reporting.fns_sum_splits.sql"
	"./functions/reporting.fns_accountvalue_assets.sql"
)

for routine in "${routines[@]}"; do

	psql -d ${gnucash_reporting_database} -U "${POSTGRES_USER}" -f "${routine}"

done

# helper views
helper_views=(
	"./views/dba.dependencies.sql"
	"./views/reporting.dateseries_monthend.sql"
	"./views/reporting.dateseries_yearend.sql"
)

for helperview in "${helper_views[@]}"; do

	psql -d ${gnucash_reporting_database} -U "${POSTGRES_USER}" -f "${helperview}"

done

# views dependendent on any of the materialized views of the base gnucash tables
reporting_views=(
	"./views/reporting.commodities_currentprices.sql"
	"./views/reporting.accounts_balances.sql"
	"./views/reporting.accounts_balances_assetsliabilities.sql"
	"./views/reporting.accounts_expense_housing.sql"
	"./views/reporting.accounts_expense_tax.sql"
	"./views/reporting.assets_by_group_monthly.sql"
	"./views/reporting.assets_by_group_monthly_13.sql"
	"./views/reporting.assets_by_group_yearly.sql"
	"./views/reporting.assets_by_group_yearly_5.sql"
	"./views/reporting.assets_by_date_group_monthly_13.sql"
	"./views/reporting.assets_by_date_group_yearly_5.sql"
	"./views/reporting.assets_liabilities_by_type.sql"
	"./views/reporting.balance_sheet.sql"
	"./views/reporting.balance_sheet_summary.sql"
	"./views/reporting.cashflow_splits.sql"
	"./views/reporting.cashflow_detail_monthly_3.sql"
	"./views/reporting.cashflow_detail_yearly_3.sql"
	"./views/reporting.eom_balances_assets_by_group.sql"
	"./views/reporting.eom_balances_assets_liabilities.sql"
	"./views/reporting.eom_balances_by_class.sql"
	"./views/reporting.eom_balances_income_expense.sql"
	"./views/reporting.eoy_balances_assets_by_group.sql"
	"./views/reporting.eoy_balances_by_class.sql"
	"./views/reporting.income_expense_detail_monthly.sql"
	"./views/reporting.income_expense_detail_monthly_3.sql"
	"./views/reporting.income_expense_detail_yearly.sql"
	"./views/reporting.income_expense_detail_yearly_3.sql"
	"./views/reporting.income_expense_retained_monthly.sql"
	"./views/reporting.income_expense_housing_average_monthly_13.sql"
	"./views/reporting.income_expense_retained_average_monthly.sql"
	"./views/reporting.income_expense_retained_average_monthly_13.sql"
	"./views/reporting.income_expense_retained_takehome_average_monthly_13.sql"
	"./views/reporting.income_expense_retained_yearly.sql"
	"./views/reporting.income_by_group_yearly.sql"
	"./views/reporting.income_detail_iradistributions.sql"
	"./views/reporting.income_detail_iradistributions_yearly.sql"
	"./views/reporting.networth_monthly.sql"
	"./views/reporting.networth_monthly_13.sql"
	"./views/reporting.networth_yearly.sql"
	"./views/reporting.networth_yearly_5.sql"
	"./views/reporting.portfolio_transactions.sql"
	"./views/reporting.portfolio_dividends_quarterly.sql"
	"./views/reporting.portfolio_dividends_yearly.sql"
)

for reportingview in "${reporting_views[@]}"; do

	psql -d ${gnucash_reporting_database} -U "${POSTGRES_USER}" -f "${reportingview}"

done

# indices
indices=(
	"./indices/reporting.indices.sql"
)

for index in "${indices[@]}"; do

	psql -d ${gnucash_reporting_database} -U "${POSTGRES_USER}" -f "${index}"

done

# '$' characters are escaped with '\' in the encrypted password string
psql -a -d ${gnucash_reporting_database} -U "${POSTGRES_USER}" -c "
CREATE ROLE ${gnucash_reporting_user} LOGIN
 ENCRYPTED PASSWORD 'SCRAM-SHA-256\$4096:mZ58odl6fcSzHgsj49SCgQ==\$KaUXJsrqWKnLUG5dJ/V3sJWOxgqgyqYBiQ5poao4Hoc=:jHNpYggVQvesu5Aoc2lnnh++R3lEjUTY6uW/385tdlE='
 NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION CONNECTION LIMIT 10;"

psql -a -d ${gnucash_reporting_database} -U "${POSTGRES_USER}" -c "
REVOKE CONNECT ON DATABASE ${gnucash_reporting_database} FROM PUBLIC;
GRANT  CONNECT ON DATABASE ${gnucash_reporting_database} TO ${gnucash_reporting_user};
REVOKE ALL     ON SCHEMA   public FROM PUBLIC;
GRANT  USAGE   ON SCHEMA   public TO ${gnucash_reporting_user};
GRANT  EXECUTE ON FUNCTION dba.fn_refresh_reporting() TO ${gnucash_reporting_user};"

for schema in "${schemas[@]}"; do

	psql -a -d ${gnucash_reporting_database} -U "${POSTGRES_USER}" -c "
	REVOKE ALL    ON SCHEMA ${schema} FROM PUBLIC;
	GRANT  USAGE  ON SCHEMA ${schema} TO ${gnucash_reporting_user};
	GRANT  SELECT ON ALL TABLES IN SCHEMA ${schema} TO ${gnucash_reporting_user};"

done
