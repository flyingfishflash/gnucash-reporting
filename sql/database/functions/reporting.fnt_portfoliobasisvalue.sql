-- Function: reporting.fnt_portfoliobasisvalue(date)

-- DROP FUNCTION reporting.fnt_portfoliobasisvalue(date);

CREATE OR REPLACE FUNCTION reporting.fnt_portfoliobasisvalue(IN asofdate date DEFAULT (now())::date)
RETURNS TABLE(date date, basis numeric, value numeric) AS

$BODY$

/*

	It's important to note that:
	1) Basis is calculated using a gnucash populated 'value' field in the [splits] table
	2) Value is calculated using the closest (market value) price by date in the [prices] table

	The reason the price list is not used when calculating basis is that some shares may have been purchased
	at less than market value (employee stock purchase plan, etc).

*/

/*
with cte_basis
as (
	select
		sum(cast(s.value as numeric(18,8))) as basis
		
	from reporting.splits s
		inner join reporting.accounts a
			on s.account_guid = a.guid
		inner join reporting.commodities c
			on c.guid = a.commodity_guid
	
	where
		s.post_date::date <= asofdate::date --@asofdate
		and a.account_type in ('MUTUAL','FUND','STOCK')
),

cte_portfoliobalances
as (
	select
		a.guid
	,	a.commodity_guid
	,	sum(cast(s.quantity as numeric(18,8))) * (reporting.fnt_nearestcommodityprice(a.commodity_guid::varchar, asofdate::date, -3)).price as balance
	
	from reporting.splits s
		inner join reporting.accounts a
			on s.account_guid = a.guid
		inner join reporting.commodities c
			on c.guid = a.commodity_guid
		
	where
		s.post_date::date <= asofdate::date --@asofdate
		and a.account_type in ('MUTUAL','FUND','STOCK')
			
	group by
		a.guid
	,	a.commodity_guid

),

cte_value
as (
	select
		sum(balance) as value

	from cte_portfoliobalances pb
)

select
	$1::date as date
,	basis
,	value

from cte_basis, cte_value;
*/



	with cteportfoliotransactionsbydate
	as (
		select
			a.intid as a_intid
		,	a.commodity_guid as c_guid
		,	cast(s.post_date as date) as post_date
		,	sum(cast(s.quantity as decimal(18,8))) as quantity
		,	sum(cast(s.value as decimal(18,8))) as value
		
		from reporting.splits s
			inner join reporting.accounts a
				on s.account_guid = a.guid
			inner join reporting.commodities c
				on c.guid = a.commodity_guid
		
		where
			s.post_date::date <= $1::date --@asofdate
			--and a.guid = $1
			and a.account_type in ('MUTUAL','FUND','STOCK')
			and s.quantity_num <> 0
			
		group by
			a.intid
		,	post_date
		,	a.commodity_guid
	),

	cteportfoliotransactions
	as (
	select
	*
	,	(value/quantity) as price
	
	from cteportfoliotransactionsbydate
	),

	
	ctestocksum
	as (
		select
			a.intid as a_intid
		,	sum(quantity) as balance
		
		from cteportfoliotransactions pt
			left join reporting.accounts a
				on a.intid = pt.a_intid
		
		group by a.intid
	),

	ctereverseinsum
	as (
		select
			a.name as account
		,	c.mnemonic as mnemonic
		,	cast(pt.post_date as date) as post_date
		,	(select sum((cpt.quantity))
							from cteportfoliotransactions cpt
							where
								((cpt.a_intid = pt.a_intid)
								and (cpt.quantity > 0)
								and (cpt.post_date >= cast(pt.post_date as date)))) as rollingstock
		,	pt.quantity
		,	pt.a_intid
		,	a.intid

		from cteportfoliotransactions pt
			left join reporting.accounts a
				on pt.a_intid = a.intid
			left join reporting.commodities c
				on pt.c_guid = c.guid

		where
		 pt.quantity > 0
	),

	ctewithlasttrandate
	as (
		select * from (
			select
				b.a_intid
			,	b.balance
			,	lastpartialstock.post_date
			,	lastpartialstock.stocktouse
			,	lastpartialstock.runningtotal
			,	(b.balance - lastpartialstock.runningtotal + lastpartialstock.stocktouse) as usethisstock
			,	rank() over (partition by a_intid order by post_date desc) as rank

			from ctestocksum b
				join
					(	select
							ris.post_date
						,	ris.intid
						,	ris.quantity as stocktouse
						,	ris.rollingstock as runningtotal
						from ctereverseinsum ris
					) lastpartialstock
				on b.a_intid = lastpartialstock.intid
					and lastpartialstock.runningtotal >= b.balance
		) withlasttrandate

		where rank = 1
	),

	cteportfolioaccountbalances
	as (
		select
			wltd.a_intid
		,	wltd.balance as balance
		--,	sum(case when pt.post_date = wltd.post_date then wltd.usethisstock else pt.quantity end * ((select (reporting.fnt_nearestcommodityprice(pt.c_guid, pt.post_date::date, -3)).price))) as basis
		,	sum(case when pt.post_date = wltd.post_date then wltd.usethisstock else pt.quantity end * pt.price) as basis

		from ctewithlasttrandate wltd
			inner join cteportfoliotransactions pt
				on pt.a_intid = wltd.a_intid
					and pt.post_date >= wltd.post_date
					and pt.quantity > 0
			left join reporting.accounts a
				on a.intid = pt.a_intid

		group by
			  wltd.a_intid
			, wltd.balance
	),

	cteportfoliobasisvalue
	as (
		select
			$1::date as date --[date] = @asofdate
		--,	account = a.name
		,	pab.basis as basis
		--,	balance = pab.balance	
		--,	price = dbo.fnsnearestcommodityprice(a.commodity_guid, @asofdate, null)
		,	(select (reporting.fnt_nearestcommodityprice(a.commodity_guid, $1::date, -3)).price) * pab.balance as value
		,	a.commodity_guid

		from cteportfolioaccountbalances pab
			inner join reporting.accounts a
				on a.intid = pab.a_intid

		)

	select
		date
	,	sum(basis) as basis
	,	sum(value) as value

	from cteportfoliobasisvalue

	group by
		date;

	--order by
	--	date

$BODY$

LANGUAGE sql VOLATILE
COST 100
ROWS 1;
