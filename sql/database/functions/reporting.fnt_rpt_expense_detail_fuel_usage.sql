-- Function: reporting.fnt_rpt_expense_detail_fuel_usage()

-- DROP FUNCTION reporting.fnt_rpt_expense_detail_fuel_usage();

CREATE OR REPLACE FUNCTION reporting.fnt_rpt_expense_detail_fuel_usage(/*IN beginningmileage integer*/)
RETURNS TABLE("row" bigint, intid bigint, longname character varying, name character varying, post_date date, value numeric, mileage integer, volume numeric, price numeric, milestravelled integer, milespergallon numeric) AS

$BODY$

begin

	return query

	with cte_fuelusage as
	(
		select
			row_number() over(order by s.post_date) as row
		,	a.IntID
		,	a.LongName
		,   a.Name
		,	CAST(s.Post_Date as Date) as Post_Date
		,	CAST(s.Value as numeric(18,8)) as Value
		,	coalesce(cast((select a[2] from (select regexp_split_to_array(s.description, ':')) as dt(a)) as integer),0) as Mileage
		,	coalesce(cast((select a[3] from (select regexp_split_to_array(s.description, ':')) as dt(a)) as numeric(10,4)),0) as Volume
		,	cast(s.Value / (coalesce(cast((select a[3] from (select regexp_split_to_array(s.description, ':')) as dt(a)) as numeric(10,4)),0)) as numeric (10,4)) as Price

		from reporting.accounts a
			left join reporting.splits s on 
				s.account_guid = a.guid

		where
			a.longname like '%Expenses%Fuel%'
			and s.description like '%:%'

		order by
			post_date
	)

	select
	*
	, 	case
			/*when fu1.Row = 1 then coalesce((fu1.Mileage - $1),0)*/
			when fu1.Row = 1 then coalesce((fu1.Mileage - 0),0)
			else coalesce(fu1.Mileage - (select (fu2.Mileage) from cte_fuelusage fu2 where fu2.row = fu1.row - 1), 0)
		end as MilesTraveled
	, 	cast(case
			/*when fu1.Row = 1 then coalesce((fu1.Mileage - $1),0) / fu1.Volume*/
			when fu1.Row = 1 then coalesce((fu1.Mileage - 0),0) / fu1.Volume
			else coalesce(fu1.Mileage - (select (fu2.Mileage) from cte_fuelusage fu2 where fu2.row = fu1.row - 1), 0) / fu1.Volume
		end as numeric) as MilesPerGallon
	from cte_fuelusage fu1;

end;

$BODY$

LANGUAGE plpgsql VOLATILE
COST 100
ROWS 1000;
