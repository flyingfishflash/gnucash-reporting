-- View: reporting.accounts_balances

-- DROP VIEW reporting.accounts_balances;

CREATE OR REPLACE VIEW reporting.accounts_balances AS 

WITH cte_accounts AS (

SELECT
	a.intid
	, a.guid
	, a.code
	, a.depth
	, a.commodity_mnemonic
	, a.name
	, a.longname
	, a.lft
	, a.rgt
	, a.account_class
	, a.account_type
	, a.hidden
	, a.parent_guid
	, ccp.pricedate::date AS pricedate
	, ccp.price
	
FROM reporting.accounts a
    LEFT JOIN reporting.commodities_currentprices ccp
	    ON a.commodity_guid::text = ccp.commodity_guid::text

),
        
cte_splits_quanitity_by_account AS (

SELECT
	s.account_guid,
	sum(s.quantity) AS balance

FROM reporting.splits s

GROUP BY
	s.account_guid

),

cte_account_balance_value AS (

SELECT
	ca.intid
	, ca.guid
	, ca.code
	, ca.depth
	, ca.commodity_mnemonic
	, ca.name
	, ca.longname
	, ca.lft
	, ca.rgt
	, ca.account_class
	, ca.account_type
	, ca.hidden
	, ca.parent_guid
	, ca.pricedate
	, ca.price
	, COALESCE(csqba.balance, 0::numeric) AS balance
	, COALESCE(csqba.balance * COALESCE(ca.price, 1::numeric), 0::numeric) AS value

FROM cte_accounts ca
		 LEFT JOIN cte_splits_quanitity_by_account csqba
			ON csqba.account_guid::text = ca.guid::text

)

SELECT
    cte_account_balance_value.intid
    , cte_account_balance_value.guid
    , cte_account_balance_value.code
    , cte_account_balance_value.depth
    , cte_account_balance_value.commodity_mnemonic
    , cte_account_balance_value.name
    , cte_account_balance_value.longname
    , cte_account_balance_value.lft
    , cte_account_balance_value.rgt
    , cte_account_balance_value.account_class
    , cte_account_balance_value.account_type
    , cte_account_balance_value.hidden
    , cte_account_balance_value.parent_guid
    , cte_account_balance_value.pricedate
    , cte_account_balance_value.price
    , cte_account_balance_value.balance
    , cte_account_balance_value.value

FROM cte_account_balance_value;