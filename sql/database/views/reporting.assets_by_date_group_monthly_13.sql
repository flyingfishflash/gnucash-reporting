﻿-- View: reporting.assets_by_date_group_monthly_13

-- DROP VIEW reporting.assets_by_date_group_monthly_13;

CREATE OR REPLACE VIEW reporting.assets_by_date_group_monthly_13 AS 

WITH cte_crosstab AS (

SELECT
	  crosstab.assettype
	, crosstab.accountgroup
	, crosstab.month13
	, crosstab.month12
	, crosstab.month11
	, crosstab.month10
	, crosstab.month9
	, crosstab.month8
	, crosstab.month7
    , crosstab.month6
    , crosstab.month5
    , crosstab.month4
    , crosstab.month3
    , crosstab.month2
    , crosstab.month1

FROM reporting.crosstab('select accountgroup, assettype, monthenddate, accountgroupvalue from reporting.eom_balances_assets_by_group order by code'::text, 'select distinct monthenddate from reporting.eom_balances_assets_by_group where monthenddate > (now() - ''14 months''::interval) order by monthenddate desc limit 13'::text) crosstab(accountgroup character varying, assettype character varying, month1 numeric, month2 numeric, month3 numeric, month4 numeric, month5 numeric, month6 numeric, month7 numeric, month8 numeric, month9 numeric, month10 numeric, month11 numeric, month12 numeric, month13 numeric)

)

SELECT
          cte_crosstab.assettype
	, cte_crosstab.accountgroup
	, cte_crosstab.month13
	, cte_crosstab.month12
	, cte_crosstab.month11
	, cte_crosstab.month10
	, cte_crosstab.month9
	, cte_crosstab.month8
	, cte_crosstab.month7
	, cte_crosstab.month6
	, cte_crosstab.month5
	, cte_crosstab.month4
	, cte_crosstab.month3
	, cte_crosstab.month2
	, cte_crosstab.month1
	, cte_crosstab.month1 - cte_crosstab.month13 AS fluctuation

FROM cte_crosstab;
