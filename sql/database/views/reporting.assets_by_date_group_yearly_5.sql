﻿-- View: reporting.assets_by_date_group_yearly_5

-- DROP VIEW reporting.assets_by_date_group_yearly_5;

CREATE OR REPLACE VIEW reporting.assets_by_date_group_yearly_5 AS 

WITH cte_crosstab AS (

SELECT
	  crosstab.assettype
	, crosstab.accountgroup
	, crosstab.year5
	, crosstab.year4
	, crosstab.year3
	, crosstab.year2
	, crosstab.year1

FROM reporting.crosstab('select accountgroup, assettype, monthenddate, accountgroupvalue from reporting.eoy_balances_assets_by_group order by code'::text, 'select distinct monthenddate from reporting.eoy_balances_assets_by_group where monthenddate > (now() - ''4 years''::interval) order by monthenddate'::text) crosstab(accountgroup character varying, assettype character varying, year5 numeric, year4 numeric, year3 numeric, year2 numeric, year1 numeric)

)

SELECT
          cte_crosstab.assettype
	, cte_crosstab.accountgroup
	, cte_crosstab.year5
	, cte_crosstab.year4
	, cte_crosstab.year3
	, cte_crosstab.year2
	, cte_crosstab.year1
	, cte_crosstab.year1 - cte_crosstab.year5 AS fluctuation

FROM cte_crosstab;
