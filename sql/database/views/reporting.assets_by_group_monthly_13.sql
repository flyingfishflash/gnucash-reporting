-- View: reporting.assets_by_group_monthly_13

-- DROP VIEW reporting.assets_by_group_monthly_13;

CREATE OR REPLACE VIEW reporting.assets_by_group_monthly_13 AS 

WITH cte_row_number AS (

SELECT
	row_number() OVER (ORDER BY assets_by_group_monthly.date) AS row_number
	, assets_by_group_monthly.date
	, assets_by_group_monthly.assetsvalue
	, assets_by_group_monthly.fluctuation_assetsvalue
	, assets_by_group_monthly.cash
	, assets_by_group_monthly.fluctuation_cash
	, assets_by_group_monthly.bank
	, assets_by_group_monthly.fluctuation_bank
	, assets_by_group_monthly.bonds
	, assets_by_group_monthly.fluctuation_bonds
	, assets_by_group_monthly.ira
	, assets_by_group_monthly.fluctuation_ira
	, assets_by_group_monthly."401k"
	, assets_by_group_monthly.fluctuation_401k
	, assets_by_group_monthly.hsa
	, assets_by_group_monthly.fluctuation_hsa
	, assets_by_group_monthly.investments
	, assets_by_group_monthly.fluctuation_investments
	, assets_by_group_monthly.suspense
	, assets_by_group_monthly.fluctuation_suspense
	, assets_by_group_monthly.vehicles
	, assets_by_group_monthly.fluctuation_vehicles

FROM reporting.assets_by_group_monthly

WHERE
	assets_by_group_monthly.date > (now() - '1 year'::interval)
	
)

SELECT
	cte_row_number.date
	, cte_row_number.assetsvalue
	, CASE
		WHEN cte_row_number.row_number = 1 THEN 0::numeric
        ELSE cte_row_number.fluctuation_assetsvalue
      END AS fluctuation_assetsvalue
	, cte_row_number.cash
	, CASE
        WHEN cte_row_number.row_number = 1 THEN 0::numeric
        ELSE cte_row_number.fluctuation_cash
      END AS fluctuation_cash
	, cte_row_number.bank
	, CASE
		WHEN cte_row_number.row_number = 1 THEN 0::numeric
        ELSE cte_row_number.fluctuation_bank
      END AS fluctuation_bank
    , cte_row_number.bonds
    , CASE
		WHEN cte_row_number.row_number = 1 THEN 0::numeric
        ELSE cte_row_number.fluctuation_bonds
      END AS fluctuation_bonds
    , cte_row_number.ira
    , CASE
		WHEN cte_row_number.row_number = 1 THEN 0::numeric
        ELSE cte_row_number.fluctuation_ira
      END AS fluctuation_ira
    , cte_row_number."401k"
    , CASE
        WHEN cte_row_number.row_number = 1 THEN 0::numeric
        ELSE cte_row_number.fluctuation_401k
      END AS fluctuation_401k
    , cte_row_number.hsa
    , CASE
        WHEN cte_row_number.row_number = 1 THEN 0::numeric
        ELSE cte_row_number.fluctuation_hsa
      END AS fluctuation_hsa
    , cte_row_number.investments
    , CASE
        WHEN cte_row_number.row_number = 1 THEN 0::numeric
        ELSE cte_row_number.fluctuation_investments
      END AS fluctuation_investments
    , cte_row_number.suspense
    , CASE
        WHEN cte_row_number.row_number = 1 THEN 0::numeric
        ELSE cte_row_number.fluctuation_suspense
      END AS fluctuation_suspense
    , cte_row_number.vehicles
    , CASE
        WHEN cte_row_number.row_number = 1 THEN 0::numeric
        ELSE cte_row_number.fluctuation_vehicles
      END AS fluctuation_vehicles

FROM cte_row_number;
