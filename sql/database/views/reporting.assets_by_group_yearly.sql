-- View: reporting.assets_by_group_yearly

-- DROP VIEW reporting.assets_by_group_yearly;

CREATE OR REPLACE VIEW reporting.assets_by_group_yearly AS 

WITH cte_crosstab_assetvalues AS (

SELECT
	ct.date
	, ct.cash
	, ct.bank
	, ct.bonds
	, ct.ira
	, ct."401k"
	, ct.hsa
	, ct.investments
	, ct.suspense
	, ct.vehicles

FROM reporting.crosstab('select monthenddate, accountgroup, accountgroupvalue from reporting.eoy_balances_assets_by_group'::text)
	ct(date date, cash numeric, bank numeric, bonds numeric, ira numeric, "401k" numeric, hsa numeric, investments numeric, suspense numeric, vehicles numeric)

),

cte_assetvaluesovertime AS (

SELECT
	cte_crosstab_assetvalues.date
	, cte_crosstab_assetvalues.cash
	, cte_crosstab_assetvalues.bank
	, cte_crosstab_assetvalues.bonds
	, cte_crosstab_assetvalues.ira
	, cte_crosstab_assetvalues."401k"
	, cte_crosstab_assetvalues.hsa
	, cte_crosstab_assetvalues.investments
	, cte_crosstab_assetvalues.suspense
	, cte_crosstab_assetvalues.vehicles
	, cte_crosstab_assetvalues.cash
	  + cte_crosstab_assetvalues.bank
	  + cte_crosstab_assetvalues.bonds
	  + cte_crosstab_assetvalues.ira
	  + cte_crosstab_assetvalues."401k"
	  + cte_crosstab_assetvalues.hsa
	  + cte_crosstab_assetvalues.investments
	  + cte_crosstab_assetvalues.suspense
	  + cte_crosstab_assetvalues.vehicles AS assetsvalue

FROM cte_crosstab_assetvalues

)

SELECT
	cte_assetvaluesovertime.date
	, cte_assetvaluesovertime.assetsvalue
	, cte_assetvaluesovertime.assetsvalue - lag(cte_assetvaluesovertime.assetsvalue, 1, 0::numeric) OVER (ORDER BY cte_assetvaluesovertime.date) AS fluctuation_assetsvalue
	, cte_assetvaluesovertime.cash
	, cte_assetvaluesovertime.cash - lag(cte_assetvaluesovertime.cash, 1, 0::numeric) OVER (ORDER BY cte_assetvaluesovertime.date) AS fluctuation_cash
	, cte_assetvaluesovertime.bank
	, cte_assetvaluesovertime.bank - lag(cte_assetvaluesovertime.bank, 1, 0::numeric) OVER (ORDER BY cte_assetvaluesovertime.date) AS fluctuation_bank
	, cte_assetvaluesovertime.bonds
	, cte_assetvaluesovertime.bonds - lag(cte_assetvaluesovertime.bonds, 1, 0::numeric) OVER (ORDER BY cte_assetvaluesovertime.date) AS fluctuation_bonds
	, cte_assetvaluesovertime.ira
	, cte_assetvaluesovertime.ira - lag(cte_assetvaluesovertime.ira, 1, 0::numeric) OVER (ORDER BY cte_assetvaluesovertime.date) AS fluctuation_ira
	, cte_assetvaluesovertime."401k"
	, cte_assetvaluesovertime."401k" - lag(cte_assetvaluesovertime."401k", 1, 0::numeric) OVER (ORDER BY cte_assetvaluesovertime.date) AS fluctuation_401k
	, cte_assetvaluesovertime.hsa
	, cte_assetvaluesovertime.hsa - lag(cte_assetvaluesovertime.hsa, 1, 0::numeric) OVER (ORDER BY cte_assetvaluesovertime.date) AS fluctuation_hsa
	, cte_assetvaluesovertime.investments
	, cte_assetvaluesovertime.investments - lag(cte_assetvaluesovertime.investments, 1, 0::numeric) OVER (ORDER BY cte_assetvaluesovertime.date) AS fluctuation_investments
	, cte_assetvaluesovertime.suspense
	, cte_assetvaluesovertime.suspense - lag(cte_assetvaluesovertime.suspense, 1, 0::numeric) OVER (ORDER BY cte_assetvaluesovertime.date) AS fluctuation_suspense
	, cte_assetvaluesovertime.vehicles
	, cte_assetvaluesovertime.vehicles - lag(cte_assetvaluesovertime.vehicles, 1, 0::numeric) OVER (ORDER BY cte_assetvaluesovertime.date) AS fluctuation_vehicles

FROM cte_assetvaluesovertime;
