-- View: reporting.assets_liabilities_by_type

-- DROP VIEW reporting.assets_liabilities_by_type;

CREATE OR REPLACE VIEW reporting.assets_liabilities_by_type AS 

SELECT
	accounts_balances_assetsliabilities.intid
	, accounts_balances_assetsliabilities.account_class
	, accounts_balances_assetsliabilities.account_type
	, accounts_balances_assetsliabilities.accountgroup
	, accounts_balances_assetsliabilities.accountgroupintid
	, accounts_balances_assetsliabilities.code
	, accounts_balances_assetsliabilities.name
	, accounts_balances_assetsliabilities.balance
	, accounts_balances_assetsliabilities.value
	, accounts_balances_assetsliabilities.pricedate

FROM reporting.accounts_balances_assetsliabilities;

