-- View: reporting.balance_sheet_summary

-- DROP VIEW reporting.balance_sheet_summary;

CREATE OR REPLACE VIEW reporting.balance_sheet_summary AS

WITH account_super_class AS (
SELECT
	row_number() OVER (ORDER BY base.account_super_class) AS seq
	, base.account_super_class AS item
	, sum(base.balance) AS value
FROM ( SELECT
		CASE WHEN balance_sheet.account_class::text = 'ASSETS'::text THEN 'Assets'::text	ELSE 'Liabilities and Equity'::text	END AS account_super_class
		, balance_sheet.account_class
		, balance_sheet.sequence
		, balance_sheet.intid
		, balance_sheet.parentaccounts
		, balance_sheet.name
		, balance_sheet.balance
      FROM reporting.balance_sheet ) base
GROUP BY base.account_super_class
),

imbalance AS (
SELECT
	3 AS seq
	, 'Imbalance'::text AS item
	, (( SELECT account_super_class.value FROM account_super_class WHERE account_super_class.item = 'Assets'::text LIMIT 1))
	- (( SELECT account_super_class.value FROM account_super_class WHERE account_super_class.item = 'Liabilities and Equity'::text LIMIT 1)) AS value
)
 
SELECT
 	account_super_class.seq
 	, account_super_class.item
 	, account_super_class.value
FROM account_super_class
UNION
SELECT
	imbalance.seq
	, imbalance.item
	, imbalance.value
FROM imbalance
ORDER BY 1;
