﻿-- View: reporting.cashflow_detail_monthly_3

-- DROP VIEW reporting.cashflow_detail_monthly_3;

CREATE OR REPLACE VIEW reporting.cashflow_detail_monthly_3 AS 

SELECT
      ct.flow
    , ct.grouptree
    --, ct.grouptreeintid
    , COALESCE(ct.month0, 0::numeric) AS month0
    , COALESCE(ct.month1, 0::numeric) AS month1
    , COALESCE(ct.month2, 0::numeric) AS month2

FROM reporting.crosstab('select grouptree, grouptreeintid, flow, monthenddate::varchar, sum(value) as balance
                         from reporting.cashflow_splits
                         where monthenddate::date > reporting.fns_lastdaymonth((reporting.fns_lastdaymonth(now()::date) - ''3 mons''::interval)::date)
                         group by flow, grouptree, grouptreeintid, monthenddate
                         order by 3, 1, 4'::text, 
                        'select distinct monthenddate from reporting.dateseries_monthend
                         where monthenddate::date > reporting.fns_lastdaymonth((reporting.fns_lastdaymonth(now()::date) - ''3 mons''::interval)::date)
                         order by 1 desc'::text)
                         ct(grouptree character varying, grouptreeintid bigint, flow character varying, month0 numeric, month1 numeric, month2 numeric)

WHERE
    ct.month0 <> 0::numeric
    OR ct.month1 <> 0::numeric
    OR ct.month2 <> 0::numeric

ORDER BY
      ct.flow
    , ct.grouptreeintid
    , ct.grouptree;
