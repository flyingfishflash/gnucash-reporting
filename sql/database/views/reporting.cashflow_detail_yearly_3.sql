﻿-- View: reporting.cashflow_detail_yearly_3

-- DROP VIEW reporting.cashflow_detail_yearly_3;

CREATE OR REPLACE VIEW reporting.cashflow_detail_yearly_3 AS 

SELECT
      ct.flow
    , ct.grouptree
    --, ct.grouptreeintid
    , COALESCE(ct.year0, 0::numeric) AS year0
    , COALESCE(ct.year1, 0::numeric) AS year1
    , COALESCE(ct.year2, 0::numeric) AS year2

FROM reporting.crosstab('select grouptree, grouptreeintid, flow, yearenddate::varchar, sum(value) as balance
                         from reporting.cashflow_splits
                         where yearenddate::date > (date_trunc(''year''::text, now()) - ''2 year - 1 days''::interval)::date
                         group by flow, grouptree, grouptreeintid, yearenddate
                         order by 3, 1, 4'::text, 
                        'select yearenddate from reporting.dateseries_yearend
                         where yearenddate::date > (date_trunc(''year''::text, now()) - ''2 year - 1 days''::interval)::date
                         order by 1 desc'::text)
                         ct(grouptree character varying, grouptreeintid bigint, flow character varying, year0 numeric, year1 numeric, year2 numeric)

WHERE
    ct.year0 <> 0::numeric
    OR ct.year1 <> 0::numeric
    OR ct.year2 <> 0::numeric

ORDER BY
      ct.flow
    , ct.grouptreeintid
    , ct.grouptree;
