﻿-- View: reporting.cashflow_split

-- DROP VIEW reporting.cashflow_splits;

CREATE OR REPLACE VIEW reporting.cashflow_splits AS 

with splits_asset_in as
(

    -- inbound split in an asset account type 
    -- is defined as a split where the value is positive

    select
      s.tx_guid as si_tx_guid
    , a.account_class as si_account_class

    from reporting.splits s
        inner join reporting.accounts a
            on s.account_guid = a.guid

    where
        a.account_class = 'ASSET'
        and s.value_num > 0


),

splits_source_distinct as
(

    -- a distinct list of the opposing splits of those in the 
    -- asset_in_splits cte for which the destination account
    -- is not an 'ASSET' class

    select distinct
      os.value
    , os.post_date
    , os.guid
    , 'IN' as flow
    , os.account_guid

    from splits_asset_in si
        left join reporting.splits os
            on si.si_tx_guid = os.tx_guid
                and os.value < 0
        inner join reporting.accounts oa
            on os.account_guid = oa.guid
                and oa.account_class <> si.si_account_class

    where
        oa.longname not like '%401k%Dividends%'
        and oa.longname not like '%HSA%Dividends%'
        and oa.longname not like '%IRA%Dividends%'
        and oa.longname not like '%Realized Gain%'
        and oa.longname not like '%Depreciation%'    
        and oa.longname not like '%Mortgage%Loans%'    

),

splits_asset_out as
(

    -- outbound split from an asset account type 
    -- is defined as a split where the value is negative

    select
      s.tx_guid as so_tx_guid
    , a.account_class as so_account_class

    from reporting.splits s
        inner join reporting.accounts a
            on s.account_guid = a.guid

    where
        a.account_class = 'ASSET'
        and s.value_num < 0

),

splits_destination_distinct as
(

    -- a distinct list of the opposing splits of those in the 
    -- asset_out_splits cte for which the destination account
    -- is not an 'ASSET' class

    select distinct
      os.value
    , os.post_date
    , os.guid
    , 'OUT' as flow
    , os.account_guid

    from splits_asset_out so
        left join reporting.splits os
            on so.so_tx_guid = os.tx_guid
                and os.value > 0
        inner join reporting.accounts oa
            on os.account_guid = oa.guid
                and oa.account_class <> so.so_account_class
    where
        oa.longname not like '%Realized Gain%'    
        and oa.longname not like '%Depreciation%'    

), 

cashflow_in_out as
(

    select
      ssd.flow
    , ssd.post_date
    , ssd.value
    , ssd.guid
    , ssd.account_guid
    , 4 as grouptreedepth
    
    from splits_source_distinct ssd

    union

    select
      sdd.flow
    , sdd.post_date
    , sdd.value
    , sdd.guid
    , sdd.account_guid
    , 4 as grouptreedepth

    from splits_destination_distinct sdd
)

select
  cf.flow
, reporting.fns_lastdaymonth(cf.post_date::date) as monthenddate
, (date_trunc('year', cf.post_date) + interval '1 year - 1 day')::date as yearenddate
, cf.post_date
, cf.value
, a.longname
, array_to_string(array
    (select
     name
     from reporting.accounts a1 WHERE a1.lft <= a.lft and a1.rgt >= a.rgt and a1.depth <= cf.grouptreedepth
     order by a1.lft), '>'
    ) as grouptree
, grouptreedepth
, (select max(intid) from reporting.accounts ai
   where ai.lft <= a.lft and ai.rgt >= a.rgt and ai.depth <= cf.grouptreedepth
  ) as grouptreeintid

from cashflow_in_out cf
    left join reporting.accounts a
        on cf.account_guid = a.guid



-- This can also be reproduced more succinctly,
-- at a slight performance cost

/*

with cashflow_assets_in_out as
(
        select
                  othersplits.longname
                , othersplits.value
                , othersplits.post_date
                , othersplits.os_s_guid

                from reporting.splits s
                        inner join reporting.accounts a
                                on s.account_guid = a.guid
                        inner join
                        ( select 
                          os.value, os.post_date, os.guid as os_s_guid, os.tx_guid, oa.longname
                          from reporting.splits os
                                inner join reporting.accounts oa
                                        on os.account_guid = oa.guid
                          where oa.account_class <> 'ASSET'
                        ) othersplits
                                on s.tx_guid = othersplits.tx_guid
                        

                where
                        a.account_class = 'ASSET'
                        and (s.value > 0 and othersplits.value < 0
                        or s.value < 0 and othersplits.value > 0)
)

select distinct
        case 
                when cf.value < 0 then 'IN'
                when cf.value > 0 then 'OUT'
        end
        , reporting.fns_lastdaymonth(cf.post_date::date) as monthenddate
        , cf.post_date
        , cf.value
        , cf.longname
        , cf.os_s_guid

from cashflow_assets_in_out cf


*/

