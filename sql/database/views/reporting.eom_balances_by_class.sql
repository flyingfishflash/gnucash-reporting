-- View: reporting.eom_balances_by_class

-- DROP VIEW reporting.eom_balances_by_class;

CREATE OR REPLACE VIEW reporting.eom_balances_by_class AS
 
WITH cte_splits_sum AS (

SELECT
	a.account_class
	, reporting.fns_lastdaymonth(s.post_date::date) AS lastdaymonth
	, a.commodity_guid AS c_guid
	, a.commodity_mnemonic
	, sum(s.quantity) AS transactionbalance

FROM reporting.splits s
	JOIN reporting.accounts a
	ON s.account_guid::text = a.guid::text

WHERE
	a.account_class::text = ANY (ARRAY['ASSET'::character varying::text, 'LIABILITY'::character varying::text])

GROUP BY
	a.account_class
	, reporting.fns_lastdaymonth(s.post_date::date)
	, a.commodity_guid, a.commodity_mnemonic

),

cte_account_classes AS (

SELECT DISTINCT
	accounts.account_class

FROM reporting.accounts

WHERE
	accounts.account_class::text = ANY (ARRAY['ASSET'::character varying::text, 'LIABILITY'::character varying::text])
	
),

cte_monthenddates_commodities AS (

SELECT
	ac.account_class
	, dateseries_monthend.monthenddate
	, c.guid AS c_guid
	, COALESCE(css.transactionbalance, 0::numeric) AS transactionbalance

FROM reporting.dateseries_monthend
	CROSS JOIN cte_account_classes ac
	CROSS JOIN reporting.commodities c
	LEFT JOIN cte_splits_sum css
		ON dateseries_monthend.monthenddate = css.lastdaymonth
		AND c.guid::text = css.c_guid::text
		AND ac.account_class::text = css.account_class::text
		
),

cte_splits_sum_balance AS (

SELECT
	cte_monthenddates_commodities.account_class
	, cte_monthenddates_commodities.monthenddate
	, cte_monthenddates_commodities.c_guid
	, cte_monthenddates_commodities.transactionbalance
	, sum(cte_monthenddates_commodities.transactionbalance) OVER (PARTITION BY cte_monthenddates_commodities.account_class, cte_monthenddates_commodities.c_guid ORDER BY cte_monthenddates_commodities.monthenddate) AS accountbalance

FROM cte_monthenddates_commodities

),

cte_splits_sum_balance_value AS (

SELECT
	cssb.account_class
	, cssb.monthenddate
	, cssb.c_guid
	, cssb.transactionbalance
	, cssb.accountbalance
	, CASE
		WHEN cssb.accountbalance <> 0::numeric THEN COALESCE(( SELECT (reporting.fnt_nearestcommodityprice(cssb.c_guid, cssb.monthenddate, (-3))).price AS price), 1::numeric) * cssb.accountbalance
		ELSE 0::numeric
	  END AS accountvalue

FROM cte_splits_sum_balance cssb

)

SELECT
	cte_splits_sum_balance_value.account_class
	, cte_splits_sum_balance_value.monthenddate
	, sum(cte_splits_sum_balance_value.accountvalue) AS accountvalue

FROM cte_splits_sum_balance_value

GROUP BY
	cte_splits_sum_balance_value.account_class
	, cte_splits_sum_balance_value.monthenddate

ORDER BY
	cte_splits_sum_balance_value.account_class
	, cte_splits_sum_balance_value.monthenddate;
