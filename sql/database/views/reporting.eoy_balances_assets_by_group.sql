﻿-- View: reporting.eoy_balances_assets_by_group

-- DROP VIEW reporting.eoy_balances_assets_by_group;

CREATE OR REPLACE VIEW reporting.eoy_balances_assets_by_group AS
 
WITH cte_accounts_account_groups AS (
SELECT
	CASE
		WHEN al.depth > 4
		THEN COALESCE(
			(SELECT a_1.name
			 FROM reporting.accounts a_1
			 WHERE a_1.lft < al.lft AND a_1.rgt > al.rgt AND a_1.depth = 4
			 LIMIT 1),
			(SELECT a_1.name
			 FROM reporting.accounts_balances a_1
			 WHERE a_1.lft < al.lft AND a_1.rgt > al.rgt AND a_1.depth <= 4
			 ORDER BY a_1.lft DESC
			 LIMIT 1),
			 'nogroup'::character varying)
		ELSE al.name
	END AS accountgroup
	, CASE
		WHEN al.depth > 4
		THEN COALESCE(
			(SELECT a_1.intid
			 FROM reporting.accounts a_1
			 WHERE a_1.lft < al.lft AND a_1.rgt > al.rgt AND a_1.depth = 4
			 LIMIT 1),
			(SELECT a_1.intid
			 FROM reporting.accounts_balances a_1
			 WHERE a_1.lft < al.lft AND a_1.rgt > al.rgt AND a_1.depth <= 4
			 ORDER BY a_1.lft DESC
			 LIMIT 1))
		ELSE al.intid
	END AS accountgroupintid
	, al.commodity_guid AS c_guid
	, al.guid AS a_guid

FROM reporting.accounts al

WHERE
	al.account_class::text = 'ASSET'::text
	AND al.name::text <> 'Imbalance-USD'::text
	
),

cte_splits_sum_balance AS (

SELECT
	caag.accountgroup
	, caag.accountgroupintid
	, caag.c_guid
	, (date_trunc('year'::text, s.post_date) + '1 year'::interval - '1 day'::interval)::date AS postmonthenddate
	, sum(s.quantity) AS transactionbalance

FROM reporting.splits s

JOIN cte_accounts_account_groups caag
	ON s.account_guid::text = caag.a_guid::text

GROUP BY
	caag.accountgroup
	, caag.accountgroupintid
	, caag.c_guid
	, (date_trunc('year'::text, s.post_date) + '1 year'::interval - '1 day'::interval)::date

ORDER BY
	caag.accountgroupintid
	, caag.c_guid
	, (date_trunc('year'::text, s.post_date) + '1 year'::interval - '1 day'::interval)::date

),
        
cte_monthenddates AS (

SELECT
	dateseries_yearend.yearenddate AS monthenddate
	, dcaag.accountgroup
	, dcaag.accountgroupintid
	, dcaag.c_guid

FROM reporting.dateseries_yearend

CROSS JOIN (
	SELECT DISTINCT
		cte_accounts_account_groups.accountgroup,
		cte_accounts_account_groups.accountgroupintid,
		cte_accounts_account_groups.c_guid
	FROM cte_accounts_account_groups) dcaag

ORDER BY
	dcaag.accountgroupintid
	, dateseries_yearend.yearenddate

),
        
cte_accounts_splits_sum_balance AS (

SELECT
	cmed.accountgroup
	, cmed.accountgroupintid
	, cmed.c_guid
	, cmed.monthenddate
	, COALESCE(cssb.transactionbalance, 0::numeric) AS transactionbalance

FROM cte_monthenddates cmed

LEFT JOIN cte_splits_sum_balance cssb
	ON cmed.accountgroupintid = cssb.accountgroupintid
		AND cmed.monthenddate = cssb.postmonthenddate
		AND cmed.c_guid::text = cssb.c_guid::text

ORDER BY
	cmed.accountgroupintid
	, cmed.c_guid
	, cmed.monthenddate

),
        
cte_group_commodity_balance AS (

SELECT
	cassb.accountgroup
	, cassb.accountgroupintid
	, cassb.c_guid
	, cassb.monthenddate
	, sum(cassb.transactionbalance) OVER (PARTITION BY cassb.accountgroupintid, cassb.c_guid ORDER BY cassb.accountgroupintid, cassb.c_guid, cassb.monthenddate) AS group_commodity_balance

FROM cte_accounts_splits_sum_balance cassb

),
        
cte_group_commodity_value AS (

SELECT
	gcb.accountgroup
	, gcb.accountgroupintid
	, gcb.c_guid
	, gcb.monthenddate
	, gcb.group_commodity_balance
	, CASE
		WHEN gcb.group_commodity_balance <> 0::numeric THEN COALESCE(( SELECT (reporting.fnt_nearestcommodityprice(gcb.c_guid, gcb.monthenddate, (-3))).price AS price), 1::numeric) * gcb.group_commodity_balance
		ELSE 0::numeric
	  END AS groupcommodityvalue

FROM cte_group_commodity_balance gcb

)

SELECT
	cte_group_commodity_value.monthenddate
        , (SELECT a_1.name FROM reporting.accounts a_1 WHERE a_1.lft < a.lft AND a_1.rgt > a.rgt order by a_1.lft desc LIMIT 1) as assettype -- parent account of accountgroup	
	, a.code
        , cte_group_commodity_value.accountgroup
	, sum(cte_group_commodity_value.groupcommodityvalue) AS accountgroupvalue

FROM cte_group_commodity_value

JOIN reporting.accounts a
	ON cte_group_commodity_value.accountgroupintid = a.intid

WHERE
	a.depth = 4

GROUP BY
	cte_group_commodity_value.monthenddate
	, (SELECT a_1.name FROM reporting.accounts a_1 WHERE a_1.lft < a.lft AND a_1.rgt > a.rgt order by a_1.lft desc LIMIT 1) -- parent account of accountgroup	
	, cte_group_commodity_value.accountgroupintid
	, a.code
	, cte_group_commodity_value.accountgroup

ORDER BY
	cte_group_commodity_value.monthenddate
	, cte_group_commodity_value.accountgroupintid;
