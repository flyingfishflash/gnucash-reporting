-- View: reporting.eoy_balances_by_class

-- DROP VIEW reporting.eoy_balances_by_class;

CREATE OR REPLACE VIEW reporting.eoy_balances_by_class AS 

WITH cte_splits_sum AS (

SELECT
	a.account_class
	, (date_trunc('year'::text, s.post_date::date::timestamp with time zone) + '1 year -1 days'::interval)::date AS lastdaymonth
	, a.commodity_guid AS c_guid
	, sum(s.quantity) AS transactionbalance

FROM reporting.splits s
	JOIN reporting.accounts a ON s.account_guid::text = a.guid::text

WHERE
	a.account_class::text = ANY (ARRAY['ASSET'::character varying::text, 'LIABILITY'::character varying::text])

GROUP BY
	a.account_class
	, (date_trunc('year'::text
	, s.post_date::date::timestamp with time zone) + '1 year -1 days'::interval)::date
	, a.commodity_guid

),

cte_account_classes AS (
	
	SELECT unnest(ARRAY['ASSET'::text, 'LIABILITY'::text]) AS account_class

),

cte_monthenddates_commodities AS (

SELECT
	ac.account_class
	, dateseries_yearend.yearenddate AS monthenddate
	, c.guid AS c_guid
	, COALESCE(css.transactionbalance, 0::numeric) AS transactionbalance

FROM reporting.dateseries_yearend

	CROSS JOIN cte_account_classes ac
	CROSS JOIN reporting.commodities c
	LEFT JOIN cte_splits_sum css ON dateseries_yearend.yearenddate = css.lastdaymonth AND c.guid::text = css.c_guid::text AND ac.account_class = css.account_class::text

),

cte_splits_sum_balance AS (

SELECT
	cte_monthenddates_commodities.account_class
	, cte_monthenddates_commodities.monthenddate
	, cte_monthenddates_commodities.c_guid
	, cte_monthenddates_commodities.transactionbalance
	, sum(cte_monthenddates_commodities.transactionbalance) OVER (PARTITION BY cte_monthenddates_commodities.account_class, cte_monthenddates_commodities.c_guid ORDER BY cte_monthenddates_commodities.monthenddate) AS accountbalance

FROM cte_monthenddates_commodities

),

cte_splits_sum_balance_value AS (

SELECT
	cssb.account_class
	, cssb.monthenddate
	, cssb.c_guid
	, cssb.transactionbalance
	, cssb.accountbalance
	, CASE
		WHEN cssb.accountbalance <> 0::numeric THEN COALESCE(( SELECT (reporting.fnt_nearestcommodityprice(cssb.c_guid, cssb.monthenddate, (-3))).price AS price), 1::numeric) * cssb.accountbalance
		ELSE 0::numeric
	  END AS accountvalue

FROM cte_splits_sum_balance cssb

)

SELECT
	cte_splits_sum_balance_value.account_class
	, cte_splits_sum_balance_value.monthenddate
	, sum(cte_splits_sum_balance_value.accountvalue) AS accountvalue

FROM cte_splits_sum_balance_value

GROUP BY
	cte_splits_sum_balance_value.account_class
	, cte_splits_sum_balance_value.monthenddate

ORDER BY
	cte_splits_sum_balance_value.account_class
	, cte_splits_sum_balance_value.monthenddate;
