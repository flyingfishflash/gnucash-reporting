-- View: reporting.income_detailiradistributions_yearly

-- DROP VIEW reporting.income_detail_iradistributions_yearly;

CREATE OR REPLACE VIEW reporting.income_detail_iradistributions_yearly AS 

SELECT
	date_part('year'::text, income_detail_iradistributions.date)::integer AS date
	, sum(income_detail_iradistributions.value) AS value

FROM reporting.income_detail_iradistributions

GROUP BY
	date_part('year'::text, income_detail_iradistributions.date)::integer;
