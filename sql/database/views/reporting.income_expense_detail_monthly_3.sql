-- View: reporting.income_expense_detail_monthly_3

-- DROP VIEW reporting.income_expense_detail_monthly_3;

CREATE OR REPLACE VIEW reporting.income_expense_detail_monthly_3 AS 

SELECT
	ct.intid
	, ct.account_class AS type
        , cast(( SELECT a2.name
            FROM reporting.accounts a2
            WHERE a2.lft < a.lft AND a2.depth = 3
            ORDER BY a2.lft DESC
            LIMIT 1
          ) || '>' || a.name as character varying(2048)) AS name
	, COALESCE(ct.month0, 0::numeric) AS month0
	, COALESCE(ct.month1, 0::numeric) AS month1
	, COALESCE(ct.month2, 0::numeric) AS month2

FROM reporting.crosstab('select level4parent_a_intid, account_class, monthenddate::varchar, balance from reporting.income_expense_detail_monthly order by 1'::text, 'select distinct monthenddate from reporting.income_expense_detail_monthly order by 1 desc'::text) ct(intid bigint, account_class character varying, month0 numeric, month1 numeric, month2 numeric)

JOIN reporting.accounts a
	ON ct.intid = a.intid

WHERE
	ct.month0 <> 0::numeric
	OR ct.month1 <> 0::numeric
	OR ct.month2 <> 0::numeric;
