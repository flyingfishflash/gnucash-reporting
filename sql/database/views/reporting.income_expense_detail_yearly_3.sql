﻿-- View: reporting.income_expense_detail_yearly_3

-- DROP VIEW reporting.income_expense_detail_yearly_3;

CREATE OR REPLACE VIEW reporting.income_expense_detail_yearly_3 AS 

SELECT
    ct.intid
    , ct.account_class AS type
    , cast(
        ( SELECT a2.name
          FROM reporting.accounts a2
          WHERE a2.lft < a.lft AND a2.depth = 3
          ORDER BY a2.lft DESC
          LIMIT 1
        ) || '>' || a.name as character varying(2048)
      ) AS name
    , COALESCE(ct.year0, 0::numeric) AS year0
    , COALESCE(ct.year1, 0::numeric) AS year1
    , COALESCE(ct.year2, 0::numeric) AS year2

FROM reporting.crosstab('select detail_level_a_intid, account_class, monthenddate::varchar, balance from reporting.income_expense_detail_yearly where monthenddate > (date_trunc(''year''::text, now()) - ''2 years''::interval)::date order by 1'::text, 'select distinct monthenddate from reporting.income_expense_detail_yearly where monthenddate > (date_trunc(''year''::text, now()) - ''2 years''::interval)::date order by 1 desc'::text) ct(intid bigint, account_class character varying, year0 numeric, year1 numeric, year2 numeric)
    JOIN reporting.accounts a ON ct.intid = a.intid

WHERE
    ct.year0 <> 0::numeric
    OR ct.year1 <> 0::numeric
    OR ct.year2 <> 0::numeric;
