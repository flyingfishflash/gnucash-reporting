﻿-- View: reporting.income_expense_housing_average_monthly_13

-- DROP VIEW reporting.income_expense_housing_average_monthly_13;

CREATE OR REPLACE VIEW reporting.income_expense_housing_average_monthly_13 AS 

WITH cte_main AS (

SELECT
	m.year
	, m.month
	, m.income
	, m.incomewagestakehome AS netincome
	, m.expense
	, m.expenseaftertax
	, m.expensehousing
	, (select avg(a.incomewagestakehome) from reporting.income_expense_retained_monthly a where a.month >= m.month::date - '1 year'::interval and a.month::date <= m.month ) as incomeaverage
	, (select avg(a.expense) from reporting.income_expense_retained_monthly a where a.month >= m.month::date - '1 year'::interval and a.month::date <= m.month ) as expenseaverage
	, (select avg(a.expenseaftertax) from reporting.income_expense_retained_monthly a where a.month >= m.month::date - '1 year'::interval and a.month::date <= m.month ) as expenseaftertaxaverage
	, (select avg(a.expensehousing) from reporting.income_expense_retained_monthly a where a.month >= m.month::date - '1 year'::interval and a.month::date <= m.month ) as expensehousingaverage

FROM reporting.income_expense_retained_monthly m

WHERE
	m.month >= (reporting.fns_lastdaymonth(now()::date) - '1 year'::interval)

ORDER BY
	m.month

)

SELECT
	cte_main.year
	, cte_main.month
	, cte_main.income
	, cte_main.incomeaverage AS netincomeaverage
	, cte_main.expense
	, cte_main.expenseaftertaxaverage
	, cte_main.expensehousingaverage
	, cte_main.expensehousingaverage / cte_main.incomeaverage AS housingnetincomepercentage
	, cte_main.expensehousingaverage / cte_main.expenseaftertaxaverage AS housingnontaxexpensepercentage

FROM cte_main;