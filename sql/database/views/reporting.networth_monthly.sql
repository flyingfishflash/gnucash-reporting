-- View: reporting.networth_monthly

-- DROP VIEW reporting.networth_monthly;

CREATE OR REPLACE VIEW reporting.networth_monthly AS 

WITH cte_crosstab_assetsliabilities AS (

SELECT
	ct.date
	, ct.assets
	, ct.liabilities

FROM reporting.crosstab('select monthenddate, account_class, accountvalue from reporting.eom_balances_by_class order by 1,2'::text) ct(date date, assets numeric, liabilities numeric)

),

cte_networth AS (

SELECT
	cte_crosstab_assetsliabilities.date,
	cte_crosstab_assetsliabilities.assets,
	cte_crosstab_assetsliabilities.liabilities,
	cte_crosstab_assetsliabilities.assets + cte_crosstab_assetsliabilities.liabilities AS networth

FROM cte_crosstab_assetsliabilities

)

SELECT
	cte_networth.date,
	cte_networth.assets,
	cte_networth.assets - lag(cte_networth.assets, 1, 0::numeric) OVER (ORDER BY cte_networth.date) AS fluctuation_assets,
	- cte_networth.liabilities AS liabilities,
	(- cte_networth.liabilities) + lag(cte_networth.liabilities, 1, 0::numeric) OVER (ORDER BY cte_networth.date) AS fluctuation_liabilities,
	cte_networth.networth,
	cte_networth.networth - lag(cte_networth.networth, 1, 0::numeric) OVER (ORDER BY cte_networth.date) AS fluctuation

FROM cte_networth;
