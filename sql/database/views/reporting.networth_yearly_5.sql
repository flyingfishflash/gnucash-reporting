-- View: reporting.networth_yearly_5

-- DROP VIEW reporting.networth_yearly_5;

CREATE OR REPLACE VIEW reporting.networth_yearly_5 AS 

WITH cte_row_number AS (

SELECT
	row_number() OVER (ORDER BY networth_yearly.date) AS row_number
	, networth_yearly.date
	, networth_yearly.assets
	, networth_yearly.fluctuation_assets
	, networth_yearly.liabilities
	, networth_yearly.fluctuation_liabilities
	, networth_yearly.networth
	, networth_yearly.fluctuation

FROM reporting.networth_yearly

WHERE
	networth_yearly.date > (now() - '4 years'::interval)

)

SELECT
	cte_row_number.date
	, cte_row_number.assets
	, CASE
		WHEN cte_row_number.row_number = 1 THEN 0::numeric
		ELSE cte_row_number.fluctuation_assets
	  END AS fluctuation_assets
    , cte_row_number.liabilities
	, CASE
		WHEN cte_row_number.row_number = 1 THEN 0::numeric
		ELSE cte_row_number.fluctuation_liabilities
	  END AS fluctuation_liabilities
	, cte_row_number.networth
	, CASE
		WHEN cte_row_number.row_number = 1 THEN 0::numeric
		ELSE cte_row_number.fluctuation
	  END AS fluctuation

FROM cte_row_number;
