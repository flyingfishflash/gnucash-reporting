-- View: reporting.portfolio_transactions

-- DROP VIEW reporting.portfolio_transactions;

CREATE OR REPLACE VIEW reporting.portfolio_transactions AS 

SELECT
	a.intid AS a_intid
	, a.guid AS a_guid
	, c.guid AS c_guid
	, s.tx_guid AS t_guid
	, s.guid AS s_guid
	, a.name AS account
	, c.mnemonic
	, s.post_date::date AS post_date
	, s.quantity
	, s.value
	, (s.value / s.quantity)::numeric(18,8) AS price
	, p.price AS pricelist_price
	, CASE
		WHEN s.quantity > 0::numeric THEN s.value
		ELSE 0.00
	  END AS fundsin
	, CASE
		WHEN s.quantity < 0::numeric THEN s.value
		ELSE 0.00
	  END AS fundsout
	, CASE
		WHEN othersplits.account_type::text = 'INCOME'::text THEN s.value
		ELSE 0.00
	  END AS income
	, othersplits.account_type AS source_account_type
	, a.longname

FROM reporting.splits s
	JOIN reporting.accounts a
		ON s.account_guid::text = a.guid::text
	JOIN reporting.commodities c
		ON c.guid::text = a.commodity_guid::text
	LEFT JOIN reporting.prices p
		ON c.guid::text = p.commodity_guid::text
		AND s.post_date::date = p.date::date
	LEFT JOIN ( 
				SELECT accounts.account_type, splits.tx_guid 
				FROM reporting.accounts
				JOIN reporting.splits
					ON splits.account_guid::text = accounts.guid::text
					AND accounts.account_type::text = 'INCOME'::text) othersplits
		ON s.tx_guid::text = othersplits.tx_guid::text

WHERE
	(a.account_type::text = ANY (ARRAY['MUTUAL'::character varying::text, 'FUND'::character varying::text, 'STOCK'::character varying::text]))
	AND s.post_date::date <= now()::date
	AND s.quantity <> 0::numeric

ORDER BY
	a.intid
	, s.post_date::date
	, s.quantity DESC;
