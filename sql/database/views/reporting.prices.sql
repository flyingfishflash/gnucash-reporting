-- Materialized View: reporting.prices

-- DROP MATERIALIZED VIEW reporting.prices;

CREATE MATERIALIZED VIEW reporting.prices AS 

WITH CTE_PRICES AS (

SELECT DISTINCT
commodity_guid
, currency_guid
, date
, source
, type
, value_num
, value_denom
, round(value_num::numeric(25)/value_denom::numeric(25), 8) as price
, rank() over (partition by commodity_guid, date::date order by date desc) as pricerank

FROM gnucash.prices

)

SELECT
    prices.commodity_guid
	, prices.currency_guid
	, prices.date
	, prices.source
	, prices.type
	, prices.value_num
	, prices.value_denom
	, prices.price
	, prices.pricerank

FROM CTE_PRICES prices

WHERE
    prices.pricerank = 1

ORDER BY
    prices.date DESC
    , prices.commodity_guid

WITH DATA;

