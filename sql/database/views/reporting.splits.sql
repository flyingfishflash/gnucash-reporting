-- Materialized View: reporting.splits

-- DROP MATERIALIZED VIEW reporting.splits;

CREATE MATERIALIZED VIEW reporting.splits AS 

WITH CTE_SPLITS AS (

SELECT
s.guid
, s.tx_guid,
 account_guid
 , memo
 , action
 , reconcile_state
 , reconcile_date
 , value_num
 , value_denom
 , quantity_num
 , quantity_denom
 , lot_guid, cast(value_num AS NUMERIC(18,8)) / value_denom AS Value
 , cast(quantity_num AS NUMERIC(18,8)) / quantity_denom AS Quantity
 , t.Post_Date, t.Enter_Date, t.Description
 
 FROM gnucash.splits s
 LEFT JOIN gnucash.transactions t
    ON t.guid=s.tx_guid

)

SELECT
    splits.guid
    , splits.tx_guid
    , splits.account_guid
    , splits.memo
    , splits.action
    , splits.reconcile_state
    , splits.reconcile_date
    , splits.value_num
    , splits.value_denom
    , splits.quantity_num
    , splits.quantity_denom
    , splits.lot_guid
    , splits.value
    , splits.quantity
    , splits.post_date
    , splits.enter_date
    , splits.description

FROM CTE_SPLITS splits

WITH DATA;
