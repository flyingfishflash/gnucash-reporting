-- Materialized View: reporting.transactions

-- DROP MATERIALIZED VIEW reporting.transactions;

CREATE MATERIALIZED VIEW reporting.transactions AS 

SELECT
    transactions.guid
    , transactions.currency_guid
    , transactions.num
    , transactions.post_date
    , transactions.enter_date
    , transactions.description

FROM gnucash.transactions

WITH DATA;
