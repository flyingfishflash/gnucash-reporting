﻿/* portfolio basis reconciliation */

select
	a.intid
,	a.name
,	(reporting.fnt_portfoliobasisvalue(a.guid, '2011-07-05'::date)).basis
,	(reporting.fnt_portfoliobasisvalue(a.guid, '2011-07-05'::date)).value
,	(reporting.fnt_portfoliobasisvalue(a.guid, '2011-07-05'::date)).pricedate
from reporting.accounts a
	where account_type in ('MUTUAL', 'FUND', 'STOCK')
--	and intid = 45;

select
	a.intid
,	a.name
,	(reporting.fnt_portfoliobasisvalue(a.guid)).basis
,	(reporting.fnt_portfoliobasisvalue(a.guid)).value
,	(reporting.fnt_portfoliobasisvalue(a.guid)).pricedate
from reporting.accounts a
	where account_type in ('MUTUAL', 'FUND', 'STOCK');
	--intid = 47


--select *, value-basis as unrealized_gain from reporting.fnt_portfoliobasisvalue('2011-07-06'::date);

select * from reporting.fnt_portfoliobasisvalue('5eb065e4e55034b0c482c5718b2b951e', '2010-01-04'::date)

select account, post_date, value, * from reporting.portfoliotransactions where a_intid = 47 and post_date::date <= '2010-02-01'::date order by a_intid, post_date

select * from reporting.splits where account_guid = '5eb065e4e55034b0c482c5718b2b951e' and quantity_num <> 0

select
  account
, post_date
, quantity
, value
, price
, fundsin
, fundsout
, source_account_type
from reporting.portfoliotransactions
where a_intid = 47
--and source_account_type is NULL

/* */

select
	account_intid
,	account
,	cast(basis as numeric(18,2)) as basis
,	value
from reporting.fnt_rpt_portfolio_summary('2010-01-04'::date)
--where account_intid=47



--select * from lots where account_intid = 47;

/* */

	select
	a.name
--,	post_date
--,	cast(s.value as numeric(18,8)) as basis
,	sum(value) as basis		
	from reporting.splits s
		inner join reporting.accounts a
			on s.account_guid = a.guid
		inner join reporting.commodities c
			on c.guid = a.commodity_guid
	
	where
		--s.post_date <= asofdate::date --@asofdate
		--and
		a.account_type in ('MUTUAL','FUND','STOCK')
		and a.intid in (47)
		and s.quantity = 0 and post_date >= '2010-09-25'::date

	--order by post_date, enter_date

	group by a.name


select account , sum(basis), sum(value), sum(basis) - sum(value) from reporting.fnt_rpt_portfolio_summary() where account_intid = 47 group by account;

select * from reporting.fnt_rpt_portfolio_summary();

select * from reporting.fnt_rpt_portfolio_summary('2011-07-05'::date);

drop table lots;

select * from lots where account_intid = 45 order by lotid;

select sum(basis) from lots where account_intid = 47 and open_date <= '2010-01-04'::date;

select * from reporting.portfoliotransactions where mnemonic = 'PRSIX' and quantity <> 0

select * from reporting.portfoliotransactions where a_intid = 45 and post_date >= '2011-04-05'::date --and quantity = 0

select * from reporting.accounts where longname like '%Bene%' --and account_class = 'MUTUAL'

select * from reporting.prices where date::date = '2011-06-28'::date


select * from reporting.splits where value = 6.29