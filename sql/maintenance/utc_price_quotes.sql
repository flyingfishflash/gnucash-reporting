﻿show timezone;

--

select
p.guid
, c.mnemonic
, p.date
, now()
, p.date - interval '1 minute' * (extract (hour from p.date)::integer * 60 + 1) as newpricedate

from prices p
	inner join commodities c
		on c.guid = p.commodity_guid

where
	cast(p.date as date) > cast(now() as date)

order by
	date desc;

--

-- update prices p
-- set date = p.date - interval '1 minute' * (extract (hour from p.date)::integer * 60 + 1)
-- where cast(p.date as date) > cast(now() as date);

--

-- delete
-- from prices p
-- where
-- 	cast(p.date as date) > cast(now() as date)
